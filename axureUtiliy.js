function isArray(item) {
    return Array.isArray(item);
  }
  function findItemInStorage(arr, field, value) {
    // TODO: need to be sure that all items has text field.
    if (isArray(arr) && arr.some((item) => item[field].text === value)) {
      return true;
    }
    return false;
  }
  function addArrToStorage(key, arr) {
    if (isArray(arr)) {
      localStorage.setItem(key, JSON.stringify(arr));
    } else {
      console.log("This is not an array", arr);
    }
  }
  function getObjectFromStorage(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  function getAxureRepeaterData(repeaterName) {
    var data = $axure("@" + repeaterName).getRepeaterData();
    console.log(repeaterName, ":", data);
    return data;
  }

  function refreshAxureRepeaterData(repeaterName, data) {
    $axure("@" + repeaterName)
      .clearRepeaterData()
      .addRepeaterData(data)
      .refreshRepeater();
  }